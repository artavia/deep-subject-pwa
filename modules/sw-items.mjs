"use strict";

import { 
  el_swOps, el_swConsole, el_clearSWConsole 

  // location functionality
  , ar_hrefLength

} from './elements-variables.mjs';


import { PrintToSWConsole } from "./utility-functions.mjs";

const js_Doc = 'serviceworker.js';
// const js_Doc = 'borknaliatest.js';

const SDub_URL = `./${js_Doc}`;

const scopeObj = { scope: './' }; // this applies to each localhost and surge or github or gitlab

const SW_Deploy = () => { 
  navigator.serviceWorker.register( SDub_URL, scopeObj ).then( registered ).catch( thrown ); 
};

const registered = (reg) => { 
  
  // console.log( 'Registration scope: ', reg.scope );
  
  // console.log( 'SW registration object... : ', reg ); // ServiceWorkerRegistration{} obj.
  // reg.active , // reg.installing, // reg.onupdatefound, // reg.pushManager, // reg.scope, // reg.sync, // reg.waiting
  
  // console.log('SDub is Registered');
};

const thrown = (err) => {
  el_swConsole.innerHTML = "";
  el_swOps.style.display = "block";

  el_clearSWConsole.addEventListener( "click", () => {
    el_swConsole.innerHTML = "";
    el_swOps.style.display = "none";
  }, false ); 

  PrintToSWConsole( `<p>Serviceworker Error: <em> ${err} </em></p>` );
};

export { SW_Deploy }; 