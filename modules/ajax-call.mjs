"use strict";

import {
  // loadicon functionality
  el_loadicon , 
  
  // location functionality
  ar_new_href
  , ar_hrefLength
  , actv_Page , 

  // presentational functionality
  el_navskin
  // , el_hotelOne
  , el_authoredBy
  , el_datestamp
  , el_acConsole
  , el_dwnldAC
  , el_swOps 
  , el_swConsole 
  , el_clearSWConsole 
  , el_detailsContainer ,

  // date object functionality
  myPresentYear 
} from './elements-variables.mjs';

import { generateErrorHTMLOutput } from "./utility-functions.mjs";

let request = null;

const launchPromise = () => {
  return loadFuncPromise().then( resolvePromise ).catch( thrown );
};

const loadFuncPromise = () => {
  
  return new Promise( (resolve, reject) => {

    el_loadicon.style.display = "block";
    
    request = new XMLHttpRequest();

    if( ar_hrefLength === 4 ){ // localhost
      var url = `../model/data.json`; 
      //const url = `../model/borknaliatest.json`;
    }
    else 
    if( ar_hrefLength === 5 ){ // gitlab, github, surge
      var url = `../${ar_new_href[3]}/model/data.json`; 
      // const url = `../model/borknaliatest.json`;
    }

    request.open("GET", url, true );

    request.onload = function() {
      if (request.status == 200) {
        resolve( request.response ); // typeof request.response EQ. TO string
      }
      else {
        reject( Error(request.statusText) );
      }
    };

    request.onerror = () => {
      reject(Error("Network Error"));
      console.log("There has been a problem.");
    };
    
    request.send();
  });
};

const resolvePromise = (response) => {
  const json = JSON.parse( response );
  return json;
};

const renderHTML = ( json ) => {
  if( json !== undefined ){

    // console.log( "refax... json" , json ); 

    const { elements } = json.deepsubject.en;  // console.log( "elements" , elements );

    const {links} = elements.header[0]; // console.log( "links" , links ); // console.log( "el_navskin" , el_navskin ); 
    el_navskin.innerHTML = populateHeaderNav( links ); 

    const footerItems = elements.footer[0]; // console.log( "footerItems" , footerItems ); 
    populateFooter( footerItems ); 

    const {content} = elements; // console.log( "content" , content); 
    const { list, picture, tabs } = content;
    // console.log( "list" , list ); // console.log( "picture" , picture ); // console.log( "tabs" , tabs );
    
    // console.log( "actv_Page" , actv_Page );
    if( ( actv_Page === "" ) || ( actv_Page === "index.html" ) ){
      el_detailsContainer.insertAdjacentHTML( "beforeend" , generateList( list ) );
    }
    else
    if( actv_Page === "pop-culture.html" ){ 
      el_detailsContainer.insertAdjacentHTML( "beforeend" , generateFigures( picture ) );
    }
    else
    if( actv_Page === "personal-experience.html" ){
      el_detailsContainer.insertAdjacentHTML( "beforeend" , generateTabs( tabs ) );
    }

    // console.log( "el_hotelOne" , el_hotelOne );
    el_loadicon.style.display = "none";

  }
  return;
};

const populateHeaderNav = (arr) => {
  
  // console.log( "array of links" , arr );

  const listitems = `${arr.map( listitem ).join("")}`;
  // console.log( "listitems", listitems );

  const string = `
  <nav>
    <ul>
      ${listitems}
    </ul>
  </nav>`;

  return string;
};

const listitem = ( el, idx, array ) => {
  
  // console.log( "el object", el ); // object
  
  const key = Object.keys(el).join(""); // console.log( "key", key );

  const myvalue = el[key]; // console.log( "myvalue" , myvalue ); 

  return `<li>
    <a href=".${myvalue}">
      ${key}
    </a>
  </li>`;
};

const populateFooter = (obj) => {
  // console.log( "object of footer items" , obj ); 

  const { acConsole, authoredBy, clearSWConsole, dwnldACValue } = obj;
  el_authoredBy.innerHTML = authoredBy;
  el_datestamp.insertAdjacentHTML( "beforeend" , `\u00A9 <span>${myPresentYear}</span>` ); 
  el_acConsole.innerHTML = acConsole;
  el_dwnldAC.value = dwnldACValue;
  el_clearSWConsole.value = clearSWConsole; 
};

const generateList = (obj) => {
  
  // console.log( "index page: " , obj );
  const { header_two, detail } = obj;
  // console.log( "header_two" , header_two );
  // console.log( "detail" , detail );
  let liststring = "";
  liststring = liststring + `<h2>${header_two}</h2>`;  
  const divs = `${ detail.map( listdivelement ).join("")}`;
  liststring = liststring + divs;
  
  return liststring;
};

const listdivelement = ( div, idx, arr ) => {
  
  // console.log( "div", div );
  const { blip, headline, minutae, name, photo, qualifier } = div;

  // qualifier
  const qualifiertext = (!!qualifier) ? `<span class="qualifier">(${qualifier})</span>` : ""; 

  return `<div class="detail">
    <div class="photo-column">
      <img class="detail-photo" alt="Visual representation of ${name}" src=".${photo}" />
    </div>
    <div class="info-column">
      <h3 class="name">${name} ${qualifiertext}</h3>
      <p>${blip}</p>
      <h4 class="headline-bar">${headline}</h4>
      <ul class="minutae-list">
        ${ minutae.map( (item) => { return `<li>${item}</li>` } ).join("")}
      </ul>
    </div>
  </div>`;

};

const generateFigures = (obj) => {
  
  // console.log( "pop-culture: " , obj ); 
  const {list} = obj; // console.log( "list", list );

  const { header_two, detail } = list; // console.log( "header_two" , header_two ); // console.log( "detail" , detail );

  let picturestring = "";
  picturestring = picturestring + `<h2>${header_two}</h2>`; 
  const pictures = `${ detail.map( figures ).join("")}`;
  picturestring = picturestring + pictures;
  
  return picturestring;
};

const figures = ( group, idx, arr ) => {
  
  // console.log( "group", group );
  const { firstlead, secondfigure, thirdparagraph, fourthlead, fifthfigure, sixthfinalparagraph } = group;
  
  const { imgsrcsecond, srcset1, srcset2second, srcset3, alttextsecond, captionsecond } = secondfigure;

  const { srcset1a, srcset1b, srcset2fifth, imgsrcfifth, alttextfifth, captionfifth } = fifthfigure;

  return `<p class="lead">${firstlead}</p>

  <figure>
      
    <img class="alphaset" src=".${imgsrcsecond}" srcset=".${srcset1} 1600w, .${srcset2second} 1000w, .${srcset3} 800w, .${imgsrcsecond} 500w" sizes="(max-width: 700px) 90vw, 50vw" alt="${alttextsecond}">

    <figcaption>${captionsecond}</figcaption>
  </figure>

  <p>${thirdparagraph}</p>

  <p class="lead">${fourthlead}</p>

  <figure>
    <picture>
      <source media="(min-width: 750px)" srcset=".${srcset1a} 2x, .${srcset1b}" />
      <source media="(min-width: 500px)" srcset=".${srcset2fifth}" />
      <img src=".${imgsrcfifth}" alt="${alttextfifth}">
    </picture>
    <figcaption>${captionfifth}</figcaption>
  </figure>

  <p>${sixthfinalparagraph}</p>`;

};

const generateTabs = (obj) => {
  // console.log( "personal-experience: " , obj );
  
  const { list } = obj; // console.log( "list", list );

  const { header_two, detail } = list; // console.log( "header_two" , header_two ); // console.log( "detail" , detail );

  let tabstring = `
  <h2>${header_two}</h2>
  <div class="tabs">
    ${ detail.map( tabElements ).join("")}
  </div>`;

  return tabstring; 
};

const tabElements = (group, idx, arr ) => {
  
  // console.log( "group" , group );

  const checkedexpression = (idx === 0) ? "checked" : ""; 
  const { idvalue, label, content } = group;
  
  return `<div class="tab">
    <input type="radio" name="tab-group-1" id="${idvalue}" ${checkedexpression}>
    <label for="${idvalue}">${label}</label>
    <div class="content">
      <p>${content} </p>
    </div>
  </div>`;

};

const thrown = (error) => {
  
  el_swConsole.innerHTML = "";
  el_swOps.style.display = "block";
  el_clearSWConsole.style.display = "none";
  el_swConsole.insertAdjacentHTML( "beforeend", generateErrorHTMLOutput(error) );
  

  /* console.log( "Error - error: " , error ); // Error: "Network Error"
  if (error.response) {
    // The request was made and the server responded with a status code that falls out of the range of 2xx
    console.log( "Error - error.response.data" , error.response.data );
    console.log( "Error - error.response.status" , error.response.status );
    console.log( "Error - error.response.headers" , error.response.headers );
  } 
  else 
  if (error.request) {
    // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
    console.log( "Error - error.request" , error.request );
  } 
  else {
    // Something happened in setting up the request that triggered an Error
    console.log('Error - error.message ', error.message );
  }
  console.log( "Error - error.config " , error.config ); */

  el_loadicon.style.display = "none";
};

// const generate = () => {};

export { launchPromise, renderHTML, thrown };