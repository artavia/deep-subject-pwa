"use strict";

// loadicon functionality 
const el_loadicon = document.querySelector( "#loadicon" );	
const el_head = document.getElementsByTagName( "head" )[0];

// generic functionality
const arVendorPREs = [ "moz", "ms", "webkit", "o" ];

// location functionality
const str_host = window.location.host; 
const str_href = window.location.href; 
const str_url = window.location.pathname; 
const ar_new_href = str_href.split( '/' ); 
const ar_hrefLength = ar_new_href.length;
const actv_Page = str_url.substring( str_url.lastIndexOf('/') + 1 ); 
let nm_lenThresh;

// html elements / presentational functionality
const el_navskin = document.querySelector(".navskin");
// const el_hotelOne = document.querySelector("h1");
const el_authoredBy = document.querySelector("#authoredBy");
const el_datestamp = document.querySelector("#datestamp");
const el_detailsContainer = document.querySelector("#details-container");

// date object functionality
const newDate = new Date();
const myPresentYear = newDate.getFullYear();

// service worker functionality
const el_swOps = document.querySelector("#swOps");
const el_swConsole = document.querySelector("#swConsole");
const el_clearSWConsole = document.querySelector("#clearSWConsole");

// application cache functionality
const el_acOps = document.querySelector("#acOps");
const el_acConsole = document.querySelector("#acConsole");
const el_dwnldAC = document.querySelector("#dwnldAC");

export {
  // generic functionality
  arVendorPREs , 

  // loadicon functionality 
  el_head 
  , el_loadicon ,

  // location functionality
  str_host
  , str_href
  , str_url
  , ar_new_href
  , ar_hrefLength  
  , nm_lenThresh , 
  actv_Page , 

  // presentational functionality
  el_navskin
  // , el_hotelOne
  , el_authoredBy
  , el_datestamp
  , el_acOps
  , el_acConsole
  , el_dwnldAC
  , el_swOps
  , el_swConsole
  , el_clearSWConsole
  , el_detailsContainer , 

  // date object functionality
  myPresentYear 
  
}; 