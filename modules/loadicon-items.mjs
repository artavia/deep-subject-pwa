"use strict";

// loadicon functionality 
import { js_animation , css_transform , keyframes } from './utility-functions.mjs';
import { el_head , el_loadicon } from './elements-variables.mjs'; 

const startLoadicon = () => {
  el_loadicon.style[ js_animation ] = "spin 1.406s linear infinite";	

  const KF_STR = "" +
		keyframes + " spin { "
			+ "from {" 
				+ css_transform + ":rotate( 0deg );" 
				+ " opacity: 0.4; " 
			+" }" 
			+ "50% {" 
				+ css_transform + ":rotate( 180deg );" 
				+ " opacity: 1.0;" 
			+" }" 			
			+ "to {" 
				+ css_transform + ":rotate( 360deg );" 
				+ " opacity: 0.4;" 
			+ " }" 		
		+ "}";
		
		const leFrag = document.createDocumentFragment();
		const leStyle = document.createElement( "style" );
		leStyle.type = "text/css";
		leStyle.setAttribute( "media", "all,screen,projection" );
		leStyle.appendChild( document.createTextNode( KF_STR ) ); // console.log( leStyle );
		
		leFrag.appendChild( leStyle );
		el_head.appendChild( leFrag ); // console.log( el_head );
};

export { startLoadicon };