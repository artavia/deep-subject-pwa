"use strict";

import { startLoadicon } from './loadicon-items.mjs';
import { SW_Deploy } from './sw-items.mjs';
import { LOCSTO_SETUP , AC_DEPLOY } from './appcache-items.mjs';
import { launchPromise, renderHTML, thrown } from './ajax-call.mjs';

( function() {

  "use strict"; 
  
  const domconlo = () => { 
    
    startLoadicon(); 

    if ( !( 'serviceWorker' in navigator ) && !!( 'applicationCache' in window ) ) {
      LOCSTO_SETUP();
    } 
    
  };

  const load = () => {
    const initPromise = launchPromise(); 
    initPromise.then( renderHTML ).catch( thrown ); 

    if ( !!( 'serviceWorker' in navigator ) && !!( 'applicationCache' in window ) ) { 
      SW_Deploy();
    }
    else 
    if ( !( 'serviceWorker' in navigator ) && !!( 'applicationCache' in window ) ) {
      AC_DEPLOY();
    }

  }; 

  window.addEventListener( "DOMContentLoaded" , domconlo, false );
  window.addEventListener( "load" , load, false );

} )();