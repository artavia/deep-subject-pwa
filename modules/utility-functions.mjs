"use strict";

import { arVendorPREs, el_swConsole } from './elements-variables.mjs';

// console print function
const PrintToSWConsole = ( pmAnyTxt ) => { 
  el_swConsole.insertAdjacentHTML( "beforeend", pmAnyTxt );
};

// console error function
const generateErrorHTMLOutput = (error) => {

  const errorText = ( !!error ) ? `<h4>Result</h4><h5>Error:</h5><p> ${error} </p>` : ""; 
  const ternMessageText = ( !!error.message ) ? `<h5>Message:</h5><p> ${error.message} </p>` : "";
  const ternResponseText = ( !!error.response ) ? `<h5>Status:</h5><p> ${error.response.status}: ${error.response.statusText} </p><h5>Headers:</h5><pre> ${JSON.stringify(error.response.headers, null, '\t')} </pre><h5>Data:</h5><pre> ${JSON.stringify(error.response.data, null, '\t')} </pre>` : "";
  const ternRequestText = ( !!error.request ) ? `<h5>Request:</h5><p> ${error.request} </p>` : "";
  const errorConfigText = ( !!error ) ? `<h5>Error Config:</h5><p> ${error.config} </p>` : ""; 

  const firsterror = ` ${errorText} ${ternMessageText} ${ternResponseText} ${ternRequestText} ${errorConfigText}`;
  return firsterror;
  
};

// ######################
// ##   Generic CSS Property Constructor Routine 
// ##   with pm1( CSSProp ) 
// ##   and pm2( document.createElement("div").style ) 
// ######################	

const ReturnJSProperty = ( pm1 ) => { // ( pm1, pm2 ) 
  const pm2 = document.createElement("div").style; // 
  const ar_vendorPreez = arVendorPREs; 
  const clCharMax = ar_vendorPreez.length; // 4 

  let leProp;
  let dL;
  const param = pm1; // " transform "
  const paramEl = pm2; // " document.createElement("div").style "
  const nc = param.slice( 0,1 ); // t
  const Uc = param.slice( 0,1 ).toUpperCase(); // T
  
  const Param = param.replace( nc, Uc ); // transform	
  if ( param in paramEl ) { 
    leProp = param; 
  } 
  for ( dL = 0; dL < clCharMax; dL = dL + 1) { 
    if ( ar_vendorPreez[ dL ] + Param in paramEl ) { 
      // " transform " --> msTransform, webkitTransform, oTransform, mozTransform
      leProp = ar_vendorPreez[ dL ] + Param; 
    } 
  } 
  return leProp;
};

// ######################
// ##   Return CSS Property Routine 
// ##   with pm1( CSSProp ) 
// ##   and pm2( document.createElement("div").style ) 
// ######################	

const ReturnCSSProperty = ( pm1 ) => { // ( pm1, pm2 ) 
  const dashChar = "-";
  const pm2 = document.createElement("div").style;  
  const ar_vendorPreez = arVendorPREs; 
  const clCharMax = ar_vendorPreez.length; // 3 

  let leProp;
  let dL;  
  const param = pm1; // " transform "
  const paramEl = pm2; // " document.createElement("div").style "
  const nc = param.slice( 0,1 ); // t
  const Uc = param.slice( 0,1 ).toUpperCase(); // T
  
  const Param = param.replace( nc, Uc ); // transform	
  if ( param in paramEl ) { 
    leProp = param; 
  } 
  for ( dL = 0; dL < clCharMax; dL = dL + 1) { 
    if ( ar_vendorPreez[ dL ] + Param in paramEl ) { 
      // " transform " --> -ms-transform, -webkit-transform, -o-transform, -moz-transform
      leProp = dashChar + ar_vendorPreez[ dL ] + dashChar + param; 
    } 
  } 
  return leProp;
};

// ############################
// ##   Return CSS Keyframe Animation Routine 
// ##   with pm1( CSSProp ) 
// ##   that is later helpful in obtaining 
// ##   "the ampersand + keyframes" property
// ############################

const Returnkeyframes = ( pm1 ) => { // ( pm1 ) 
  const dashChar = "-";
  const ampChar = "@";
  
  const pm2 = document.createElement("div").style; 
  const ar_vendorPreez = arVendorPREs; 
  const clCharMax = ar_vendorPreez.length; // 3 
  
  let keyframes; 
  let dL;
  const param = pm1; // "animationName" 
  const paramEl = pm2; // " document.createElement("div").style "
  const nc = param.slice( 0,1 ); // a // t
  const Uc = param.slice( 0,1 ).toUpperCase(); // A // T
  
  const Param = param.replace( nc, Uc ); // animationName 
  if ( param in paramEl ) { 
    keyframes = ampChar + "keyframes"; 
  } 
  
  for ( dL = 0; dL < clCharMax; dL = dL + 1) { 
    if ( ar_vendorPreez[ dL ] + Param in paramEl ) { 
      // " @keyframes " --> @-ms-keyframes, @-webkit-keyframes, @-o-keyframes, @-moz-keyframes
      keyframes = ampChar + dashChar + ar_vendorPreez[ dL ] + dashChar + "keyframes"; 
    } 
  } 
  
  return keyframes;
};

const js_animation = ReturnJSProperty( "animation" ); // console.log("js_animation", js_animation ); 
const css_transform = ReturnCSSProperty( "transform" ); // console.log( "css_transform" , css_transform );	
const keyframes = Returnkeyframes( "animationName" ); // console.log( "keyframes", keyframes );

export {
  js_animation
  , css_transform
  , keyframes
  , PrintToSWConsole
  , generateErrorHTMLOutput
};