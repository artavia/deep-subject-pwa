"use strict";

import { el_dwnldAC , el_acOps, el_acConsole , el_loadicon } from './elements-variables.mjs';

const AC = window.applicationCache || {}; 
// let AC; 
const acKey = "manifest";
const attrString = "ac/manifestGitLab.appcache";
const obj_localSTO = window.localStorage || {};


const LOCSTO_SETUP = () => { 
  
  if( (!obj_localSTO.manifest) && !(document.documentElement.hasAttribute( "manifest" )) ) { 
    
    obj_localSTO.setItem( acKey , attrString ); 
    document.documentElement.setAttribute( "manifest" , obj_localSTO.getItem( acKey ) );
    
    window.location.reload();
  } 
  else 
  if( (!!obj_localSTO.manifest) && !(document.documentElement.hasAttribute( "manifest" )) ) { 
    document.documentElement.setAttribute( "manifest" , obj_localSTO.getItem( acKey ) ); 
  }
};


const AC_DEPLOY = () => { 
  
  if( !!(document.documentElement.hasAttribute( "manifest" )) ){ 

    // console.log( "DEJA VU! " , document.documentElement.getAttribute('manifest') );
    
    // PHASE FINAL
    // AC = window.applicationCache || {}; 
    
    AC.addEventListener( "cached", () => {
      el_loadicon.style.display = "none";
      // console.log("AC CACHED");
    } , false );

    AC.addEventListener( "checking", () => {
      el_loadicon.style.display = "block";
      // console.log("AC CHECKING");
    } , false );

    AC.addEventListener( "downloading", () => {
      el_loadicon.style.display = "block";
      // console.log("AC DOWNLOADING");
    } , false );

    AC.addEventListener( "error", () => {
      el_dwnldAC.style.display = "none";
      el_acConsole.style.display = "none";
      // el_dwnldAC.style.display = "none";
      el_acConsole.innerHTML = "";
      // el_acOps.style.display = "block";
      el_loadicon.style.display = "none";
      // el_acConsole.innerHTML = "You probably have lost your connection. Try later.";
      // console.log("AC ERROR");
    } , false );

    AC.addEventListener( "noupdate", () => {
      
      el_loadicon.style.display = "none";
      el_acOps.style.display = "none";

      // console.log("AC NOUPDATE");

    } , false );

    AC.addEventListener( "obsolete", () => {
      el_loadicon.style.display = "none";
      // console.log("AC OBSOLETE");
    } , false );

    AC.addEventListener( "progress", () => {
      el_loadicon.style.display = "block";
      // console.log("AC PROGRESS");
    } , false );

    AC.addEventListener( "updateready", () => {
      el_loadicon.style.display = "none";
      el_acOps.style.display = "block";

      // console.log("AC UPDATEREADY");
    } , false );

    el_dwnldAC.addEventListener( "click", () => {
      if( window.confirm( "Upgrade your app please. Are you ready now? \nThis takes less than one second to complete." ) ){
        window.applicationCache.swapCache();
        window.location.reload();
      }
    }, false );

    // console.log( "AC" , AC );

  }

}; 

export { LOCSTO_SETUP , AC_DEPLOY };