# Description
Deep subject PWA by Luis.

## Please visit
You can see [the lesson at gitlab.io](https://artavia.gitlab.io/deep-subject-pwa/ "the lesson at gitlab") and decide if this is something for you to play around with at a later date.

## Complications (there aren&rsquo;t too many)
Factoring Application Cache into the equation was a bear of a task. I use an antiquated version of mobile iOS which will never be upgraded because of Apple&rsquo;s heavy-handedness in this regard. And, as we all know Application Cache is soon to go the way of the do-do bird on modern browsers. But since &quot;graceful degradation&quot; is a thing, I still need to include Application Cache functionality for the unfortunate &quot;fondle slabs&quot; (like my own) which will never get updated. 

## What it has
It makes use of several new-ish technologies including:
  - JavaScript modules (with mjs extensions);
  - JavaScript Promise object;
  - html Picture element and srcSet attribute;
  - Gulp version 4 for proper compilation which permitted me to dole out fallback code with plain old javascript extensions by means of BabelJS;
  - last, and definitely not least, Service Worker and Cache APIs.

## Attributions
I have wanted to publish something with Service Worker for some time. In fact, so much time had passed I had to essentially re-learn the finer points of the Service Worker and Cache APIs in order to get the desired results. **It was so worth it!** I borrowed heavily from at least four (4) different lessons in the following subjects:
  - css tabs lesson found at [css-tricks](https://css-tricks.com/functional-css-tabs-revisited/ "link to css-tricks article entitled Functional CSS Tabs Revisited");
  - picture element and source set lesson found at Google care of [Sam Dutton&rsquo;s lessons](https://developers.google.com/web/ilt/pwa/ "link to Sammy's PWA course") on PWAs (Progressive Web Apps);
  - Service Worker documentation at Google authored by each [Matt Gaunt](https://developers.google.com/web/fundamentals/primers/service-workers/ "link to Service Workers: an Introduction") and [Jake Archibald](https://developers.google.com/web/fundamentals/primers/service-workers/lifecycle "link to The Service Worker Lifecycle");
  - lessons on [gracefully degraded JavaScript modules](https://philipwalton.com/articles/deploying-es2015-code-in-production-today/ "link to lesson by Philip") by Philip Walton;

## For any neophytes out there&hellip;
I want to recommend the [Lighthouse](https://developers.google.com/web/tools/lighthouse/ "link to Lighthouse overview") extension which is available on Google Chrome/Chromium browsers. It helped me with curing some defects and I can thoroughly state that I benefited a lot from using it during my first real experience in publishing my first (gracefully degraded) PWA for public consumption on the web. Hip, hip&hellip;!!!

## My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!

## I thank each my Father and my Savior
My spirit couldn&rsquo;t be happier with the outcome even if I tried! Thank you for your visit! God bless you all.
