"use strict";

const appName = 'deep_subject_app';
const versionName = 'static_cache';
const majorVer = '1';
const minorVer = '00'; 
const CACHE_NAME = appName + '-' + versionName + '-v' + majorVer + '.' + minorVer; 

const toCacheRoot = [ './manifest.json', './favicon.ico', './', './index.html', './pop-culture.html', './personal-experience.html' ];
const toCacheCss = [ './css/all.min.css' ];
const toCacheMjs = [ './mjs/all.min.mjs' ];
const toCacheImgLandscape = [ './img/landscape/alive.jpg', './img/landscape/chains.jpg', './img/landscape/christ-returns.jpg', './img/landscape/el-nino.jpg', './img/landscape/faith.jpg', './img/landscape/hurricane.jpg', './img/landscape/love.jpg', './img/landscape/oil-in-lamp.jpg', './img/landscape/refining-silver.jpg', './img/landscape/rumors-of-wars.jpg', './img/landscape/shaky-ground.jpg', './img/landscape/spirit-of-cain.jpg', './img/landscape/volcano.jpg' ];
const toCacheImgPicture = [ './img/picture/believe-1600_large.jpg', './img/picture/believe-1000_large.jpg', './img/picture/believe-800_medium.jpg', './img/picture/believe-500_small.jpg', './img/picture/voice-1600_large.jpg', './img/picture/voice-1000_large.jpg', './img/picture/voice-800_medium.jpg', './img/picture/voice-500_small.jpg' ];

const filesToCache = [ ...toCacheRoot, ...toCacheCss, ...toCacheMjs, ...toCacheImgLandscape, ...toCacheImgPicture ]; 
const expectedCaches = [ CACHE_NAME ];

// The install event is the first event a service worker gets, and it only happens once.
self.addEventListener( 'install' , function InstallSDub ( event ) {	
  
  // console.log('Installation phase: Versioning ' + CACHE_NAME + '.');

  // self.skipWaiting(); 

	// A promise passed to installEvent.waitUntil() signals the duration and success or failure of your install.
	event.waitUntil( caches.open( CACHE_NAME ).then( function CacheMeOutside( cache ) { 
    
    // console.log( 'Opened cache' ); 
    // return cache.addAll( filesToCache ); 

    // https://developers.google.com/web/fundamentals/primers/service-workers/#the_defaults_of_fetch
    // opaque responses... no-cors mode
    cache.addAll( filesToCache.map( (fileToCache) => {
      return new Request( fileToCache, {mode: 'no-cors' } );
    } ) ).then( () => {
      // console.log( 'All resources have been fetched and cached.' );
    } );

	} )	); 

} , false ); 

// A service worker won't receive events like fetch and push until it successfully finishes installing and becomes "active".
self.addEventListener( 'activate' , function ActivateSDub( event ) {
  
  // delete any caches that aren't in expectedCaches
  event.waitUntil( 
    caches.keys()
    .then( (keys) => {
      return Promise.all( keys.map( (key) => {
        if( !expectedCaches.includes(key) ){
          return caches.delete(key);
        }
      } ) );
    } )
    .then( () => {
      // console.log( CACHE_NAME + ' now ready to handle fetches!');
    } ) 
  );
	
} , false );

self.addEventListener( 'fetch' , function FetchSDub( event ) {
  
  // console.log( 'fetch' , event.request );
	const url = new URL( event.request.url );
	// console.log( 'url' , url );
	// console.log( 'self.location' , self.location );

  // https://developers.google.com/web/fundamentals/primers/service-workers/#cache_and_return_requests

  event.respondWith(
    caches.match( event.request ).then( ( response ) => {
      // Cache hit - return response
      if( response ){
        return response;
      }
      // return fetch( event.request );
      
      // IMPORTANT: Clone the request. A request is a stream and can only be consumed once. Since we are consuming this once by cache and once by the browser for fetch, we need to clone the response. 

      const fetchRequest = event.request.clone();
      
      return fetch( fetchRequest ).then( ( response ) => {
        // Check if we received a valid response
        if( !response || response.status !== 200 || response.type !== 'basic' ){
          return response;
        }

        // IMPORTANT: Clone the response. A response is a stream and because we want the browser to consume the response as well as the cache consuming the response, we need to clone it so we have two streams.
        
        const responseToCache = response.clone();
        caches.open( CACHE_NAME ).then( (cache) => {
          return cache.put( event.request, responseToCache );
        } ); 
        return response;
      } );

    } ) 
  );

} , false ); 

// https://developers.google.com/web/fundamentals/primers/service-workers/