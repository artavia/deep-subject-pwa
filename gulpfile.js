const gulp = require('gulp');
const del = require( 'del' );

const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const rename = require('gulp-rename');
const connect = require('gulp-connect');
const postcss = require('gulp-postcss');

const autoprefixer = require('autoprefixer');

const imagemin = require( 'gulp-imagemin' );
const notify = require( 'gulp-notify' );
const sourcemaps = require( 'gulp-sourcemaps' );
const newer = require( 'gulp-newer' );

const cssnano = require('cssnano');
const rollup = require('gulp-better-rollup'); 
const babel = require('gulp-babel');

// const sass = require('gulp-ruby-sass'); 
const sass = require('gulp-sass'); 
// sass.compiler = require('node-sass');

const paths = { 
  
  cleanItems: ['img/','js/','mjs/','css/','styles/'], 
  indexSrc: '**/*.html', 
  imageSrc : [ 'images/**/*' ] , 
  imageDstFldr: 'img', 
  sassSrc : [ 'sass/**/*.scss' ] , 
  stylesSrc: 'styles/**/*.css' , 
  stylesFldr: 'styles', 
  cssDstFldr: 'css', 
  cssprelimNm: 'all.css', 
  cssDstFile: 'all.min.css', 

  modulesSrc: [ 'modules/**/*.?(m)js' ] , 
  moduleSrcPOE: 'modules/complete.mjs',

  jsSrc: [ 'js/polyfill.min.js', 'js/all.js' ] , 
  jsdestFold: 'js', 

  mjsdestFold: 'mjs', 
  mjsprelimNm: 'all.mjs', 
  mjsdestName: 'all.min.mjs',

  babelprelimName: 'all.js' ,
  babeldestName: 'all.min.js',

  scriptsBabelPolyfillSrc: [ 
    'node_modules/@babel/polyfill/dist/polyfill.min.js'
  ] 

}; 

gulp.task( 'watch', function( done ) {
  
  gulp.watch( paths.indexSrc , gulp.series('html') );
  gulp.watch( paths.modulesSrc , gulp.series('modulebuild') ); 
  gulp.watch( paths.sassSrc , gulp.series('cssbuild') );
  gulp.watch( paths.imageSrc , gulp.series('imgminify') ); 

  done();
});

gulp.task('connect', function(done) {
  connect.server( { 
    livereload: true 
  } );
  done();
} );

gulp.task('clean', function(done) {
  del( paths.cleanItems, done() );
} );

gulp.task( "imgminify" , function(done) {
  
  gulp.src( paths.imageSrc )
  .pipe( newer( paths.imageDstFldr ))
  .pipe( imagemin( { optimizationLevel: 3, progressive: true, interlaced: true } ) )
  .pipe( gulp.dest( paths.imageDstFldr ))
  .pipe( notify( { message: 'IMAGE MINIFICATION task is complete' } ) );
  
  done();

}); 

gulp.task('html', function(done) {
  
  gulp.src( paths.indexSrc )
  .pipe( connect.reload() )
  .pipe( notify( { message: 'HTML task complete and reloaded' } ) );

  done();
});

gulp.task( 'rollup-modules' , function(  ) {
  
  return gulp.src( paths.moduleSrcPOE )
  .pipe( sourcemaps.init() ) 
  .pipe( rollup(
    { 
      input: paths.moduleSrcPOE
    } , 
    { 
      format:'iife' 
    }
  ) ) 
  .pipe( concat( paths.mjsprelimNm ) )
  .pipe( rename( paths.mjsdestName ) )
  .pipe( uglify() )
  .pipe( sourcemaps.write() )
  .pipe( gulp.dest( paths.mjsdestFold ) )
  .pipe( connect.reload() )
  .pipe( notify( { message: 'MJS MODULE BUILD task complete and reloaded' } ) );
  
} );

gulp.task( 'babelified-compilation', function() {
  
  return gulp.src('mjs/all.min.mjs')
  .pipe( sourcemaps.init() )
  .pipe( babel( { 
    filename: '.babelrc'
    , babelrcRoots: [
      "." 
    ]
  } ) )
  .pipe( rename( paths.babelprelimName ) )
  .pipe( uglify() )
  .pipe( sourcemaps.write( './', { includeContent: false, sourceRoot: paths.jsdestFold } ) )
  .pipe( gulp.dest( paths.jsdestFold ) )
  .pipe( connect.reload() )
  .pipe( notify( { message: 'BABELIFICATION task complete and reloaded' } ) );
  
} );

gulp.task( 'js-concatenation' , function() {
  return gulp.src( paths.jsSrc )
  .pipe( sourcemaps.init() )
  .pipe( concat( paths.babeldestName ) )
  .pipe( uglify() )
  // .pipe( sourcemaps.write() )
  .pipe( sourcemaps.write( './', { includeContent: false, sourceRoot: paths.jsdestFold } ) )
  .pipe( gulp.dest( paths.jsdestFold ) )
  .pipe( connect.reload() )
  .pipe( notify( { message: 'Scripts task complete and reloaded' } ) );
} );

// gulp-ruby-sass version
/*gulp.task( 'rubysass', function() {
  
  return sass( paths.sassSrc, {sourcemap: true } )
  .on( 'error', sass.logError )
  .pipe( sourcemaps.write( './' , { includeContent: false , sourceRoot: paths.stylesFldr } ) )
  .pipe( gulp.dest( paths.stylesFldr ) )
  .pipe( connect.reload() )
  .pipe( notify( { message: 'RUBYSASS subtask complete' } ) ); 

});*/

// gulp-sass version
gulp.task( 'sass', function() {
  return gulp.src( paths.sassSrc )
    .pipe( sourcemaps.init() )
    .pipe( sass().on( 'error', sass.logError ) )
    .pipe( sourcemaps.write( './' , { includeContent: false , sourceRoot: paths.stylesFldr } ) )
    .pipe( gulp.dest( paths.stylesFldr ) )
    .pipe( connect.reload() )
    .pipe( notify( { message: 'SASS subtask complete' } ) );
});


gulp.task( 'styles', function() { 
  
  var plugins = [ 
    autoprefixer( { cascade: false } ) 
    , cssnano() 
  ];

  return gulp.src( paths.stylesSrc )
    .pipe( concat( paths.cssprelimNm ) )
    .pipe( rename( paths.cssDstFile ) )
    .pipe( postcss( plugins ) )
    .pipe( gulp.dest( paths.cssDstFldr ) )
    .pipe( connect.reload() )
    .pipe( notify( { message: 'STYLES task complete and reloaded' } ) );
});

gulp.task( 'importBabelPolyfill' , function(done){
  
  gulp.src( paths.scriptsBabelPolyfillSrc ).pipe(gulp.dest(paths.jsdestFold) ).pipe( notify( { message: 'BABEL POLYFILL has been imported' } ) ); 

  done();
} );


gulp.task( "cssbuild", gulp.series( "sass", /* "rubysass", */ "styles") );


gulp.task( "modulebuild" , gulp.series( "rollup-modules" , "babelified-compilation", "js-concatenation" ) ); 

gulp.task( 
  'default' 
  , gulp.series( 'clean', 'connect', 'importBabelPolyfill', 'watch', 'modulebuild', 'cssbuild', 'imgminify' ) 
);